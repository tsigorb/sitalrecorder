package SitalRecorder;

import static SitalRecorder.SitalRecorder.UKRecorder;
import java.io.FileOutputStream;
import java.nio.ByteBuffer;
import java.text.SimpleDateFormat;

public class CurrentCall implements Comparable<CurrentCall>
{
  byte SN,
       btCmdSrc;  // 0xFD - ������� �� ��, 0xFC - ����� �� ������� ������
  int CallId;
  long lTime,     // ����� ��������� �������
       lTm_CallStart,lTm_CallEnd,lTm_CallTalk;
  byte btChan,btOpId,btCallState,btCallType;
  String NumbSrc,NameSrc,NumbDst;
  

  final static String CallStates[]=
  {
    "Passive",            // 0
    "Call",               // 1
    "Talk",               // 2 
    "Hold",               // 3
    "Busy",               // 4
    "IncomingCall",       // 5
    "CallTest",           // 6
    "PultListen",         // 7
    "SelectorListen",     // 8
    "SelectorMain",       // 9
    "CallTestUp",         // 10
    "CallTestDown",       // 11
    "CallTestError",      // 12
    "SelectorDisableOut", // 13
    "CallError"           // 14
  };

  final static String CallTypes[]=
  {
    "Unknown",
    "TDC",                // 1
    "RSDT",               // 2
    "ATC",                // 3
    "Selector",           // 4
    "Level",              // 5
    "GroupTDC",           // 6
    "NoCall",             // 7
    "ImitatorTDC",        // 8
    "PultCall",           // 9
    "Commutator",         // 10
    "CB",                 // 11
    "GroupRSDT",          // 12
    "IndCall",            // 13
    "ImitatorRSDT",       // 14
    "Circul2_7",          // 15
    "Circul2_8",          // 16
    "Circul2_11",         // 17
    "Circul2_12",         // 18
    "PPSR",               // 19
    "GenCallTDC",         // 20
    "RD3",                // 21
    "TDCIcx",             // 22
    "Spec1",              // 23
    "Spec2",              // 24
    "Spec3",              // 25
    "Spec4",              // 26
    "Spec5",              // 27
    "Spec6",              // 28
    "Spec7",              // 29
    "Spec8",              // 30
    "ISDN"                // 31
  };
  
  CurrentCall(byte SN,byte btCmdSrc,int CallId,long lTime,
              ByteBuffer IpPckt)
  {
    this.CallId=CallId;
    this.SN=SN;
    this.btCmdSrc=btCmdSrc;
    this.lTime=lTime;
    btCallState=0;
    btCallType=0;
    btOpId=0;
    btChan=0;
    NumbSrc="";
    NameSrc="";
    NumbDst="";
    lTm_CallStart=0;
    lTm_CallEnd=0;
    lTm_CallTalk=0;
    if(IpPckt!=null)
    { // ���� ���������� � ������ ���������� � btCmdArr[12]
      btCallState=((IpPckt.get(12) & 0xff)>=CallStates.length) ? 0 : IpPckt.get(12);
      btCallType=((IpPckt.get(14) & 0xff)>=CallTypes.length) ? 0 : IpPckt.get(14);
      btOpId=IpPckt.get(15);
      btChan=IpPckt.get(16);
      try
      {
        byte btBuf[]=new byte[20];
        System.arraycopy(IpPckt.array(),17,btBuf,0,18);
        NumbSrc=(new String(btBuf, "windows-1251")).trim();
        System.arraycopy(IpPckt.array(),36,btBuf,0,20);
        if(btCallState==5) NameSrc=(new String(btBuf, "UTF-8")).trim(); // �������� �����
        else NameSrc=(new String(btBuf, "windows-1251")).trim();
        System.arraycopy(IpPckt.array(),57,btBuf,0,18);
        NumbDst=(new String(btBuf, "windows-1251")).trim();
      }
      catch(Exception e)
      { }
    }
  }

  void setCallStart()
  { lTm_CallStart=lTime; }
  
  void setCallEnd(long lTm)
  { lTm_CallEnd=lTm; }
  
  boolean setNewState(CurrentCall call0)
  {
    if(call0.btCallState==3) return false; // Hold
    if(call0.btCallState==0)
    { // Passive
      setCallEnd(call0.lTime);
      return true;
    }
    else
    {
      if(call0.btCallState==2)
      { // Talk
        if(btCallState!=2) lTm_CallTalk=call0.lTime;
      }
    }
    SN=call0.SN;
    lTime=call0.lTime;
    if(btCallState!=2) btCallState=call0.btCallState;
    btCallType=call0.btCallType;
    btOpId=call0.btOpId;
    btChan=call0.btChan;
    NumbSrc=call0.NumbSrc;
    if(NameSrc.contentEquals("")) NameSrc=call0.NameSrc;
    NumbDst=call0.NumbDst;
    return true;
  }

  String CreateInfoFName()
  {
    SimpleDateFormat sdfN=new SimpleDateFormat("yyyyMMdd");
    if(UKRecorder.bPathWithDate)
    {
      SimpleDateFormat sdfP=new SimpleDateFormat("yyyy/MM/dd");

      return UKRecorder.MessageDir+"messages/"+sdfP.format(lTm_CallStart)+
             "/SitalRecInfo"+sdfN.format(lTm_CallStart)+".txt";
    }
    else
    {
      return UKRecorder.MessageDir+"messages/SitalRecInfo"+sdfN.format(lTm_CallStart)+".txt";
    }
  }

/*
DateTime;Direction;TypeOfCall;NChan;NumberOfSrc;NameOfSrc;NumberOfDst;PultSN;OperatorChanel;ResultOfCall;TmWaitConnection;TmTalk;
���
DateTime: 20180125085837 (2018-01-25 08:58:37)
Direction: In,Out (�������� ��� ��������� �����)
TypeOfCall: ISDN,ATC,Selector,TDC,ADASE_ABNT,ADASE_ISDN,ADASE_STATION
NChan: ����� ����������� ������ (��� ISDN,ADASE_ISDN �����)
NumberOfSrc: ����� ����������� �������� (����� ���� ������)
NameOfSrc: ��� ����������� �������� (����� ���� ������)
NumberOfDst: ����� ����������� �������� (����� ���� ������)
PultSN: ����� ������
OperatorChanel: ����� ���������
ResultOfCall: Answer,NoAnswer,Busy,Error
TmWaitConnection: ����� �� ������������ ���������� (��������� ��� ����������)
TmTalk: ������������ ���������
*/
  String CreateInfoStr()
  {
    long lTmWC,lTmT;
    String str;
    SimpleDateFormat sdf=new SimpleDateFormat("yyyyMMddHHmmss");
    str=sdf.format(lTm_CallStart)+";";
    if((btCmdSrc & 0xFF)==0xFD) str+="In;";
    else str+="Out;";
    str+=CallTypes[btCallType]+";0x";
    str+=Integer.toHexString(btChan & 0xff)+";";
    str+=NumbSrc+";";
    str+=NameSrc+";";
    str+=NumbDst+";";
    str+=Integer.toString(SN)+";";
    str+=Integer.toString(btOpId)+";";
    str+=CallStates[btCallState]+";";
    if(lTm_CallTalk==0)
    {
      lTmWC=lTm_CallEnd-lTm_CallStart;
      lTmT=0;
    }
    else
    {
      lTmWC=lTm_CallTalk-lTm_CallStart;
      lTmT=lTm_CallEnd-lTm_CallTalk;
    }
    str+=Integer.toString((int)lTmWC)+";";
    str+=Integer.toString((int)lTmT)+";";
    str+="\r\n";
    return str;
  }

  boolean WriteInfo2File()
  { // ���������� ���� � ������ � ����
    String str;
    FileOutputStream fos=null;
    boolean bRet=false;

    UKRecorder.CreateRecorderDataPath(lTm_CallStart);
    try
    {
      str=CreateInfoFName();
      fos=new FileOutputStream(str, true);
      str=CreateInfoStr();
      fos.write(str.getBytes());
      bRet=true;
    }
    catch(Exception ex)
    { System.out.println("CurrentCall.write2file: "+ex.toString()); }
    finally
    {
      if(fos!=null)
      {
        try { fos.close(); }
        catch(Exception ex) { }
      }
    }
    return bRet;
  }

  @Override
  public int compareTo(CurrentCall obj)
  { return(0); }
}

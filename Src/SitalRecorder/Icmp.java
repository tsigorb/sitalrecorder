package SitalRecorder;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import com.sun.jna.Structure.ByReference;
import com.sun.jna.Structure.ByValue;
import java.util.Arrays;
import java.util.List;

public interface Icmp extends Library
{
  Icmp INSTANCE = (Icmp) Native.loadLibrary("icmp", Icmp.class);

  public static class IpAddr extends Structure
  {
    public byte[] bytes = new byte[4];

    @Override
    protected List<String> getFieldOrder()
    { return Arrays.asList(new String[]{"bytes"}); }
  }

  public static class IpAddrByVal extends IpAddr implements ByValue
  { }

  public static class IpOptionInformation extends Structure
  {
    public byte ttl;
    public byte tos;
    public byte flags;
    public byte optionsSize;
    public Pointer optionsData;

    @Override
    protected List<String> getFieldOrder()
    {
      return Arrays.asList(new String[]{"ttl","tos","flags","optionsSize","optionsData"});
    }
  }

  public static class IpOptionInformationByVal
                      extends IpOptionInformation implements ByValue
  { };

  public static class IpOptionInformationByRef
                      extends IpOptionInformation implements ByReference
  { };

  public static class IcmpEchoReply extends Structure
  {
    public IpAddrByVal address;
    public int status;
    public int roundTripTime;
    public short dataSize;
    public short reserved;
    public Pointer data;
    public IpOptionInformationByVal options;

    public IcmpEchoReply() { }

    public IcmpEchoReply(Pointer p)
    {
      useMemory(p);
      read();
    }

    @Override
    protected List<String> getFieldOrder()
    {
      return Arrays.asList(new String[]
                           {"address", "status", "roundTripTime", "dataSize",
                            "reserved", "data", "options"}
                          );
    }
  }

  public Pointer IcmpCreateFile();
  public boolean IcmpCloseHandle(Pointer hIcmp);
  public int IcmpSendEcho2(Pointer IcmpHandle, // _In_
                           Pointer Event, // _In_opt_
                           Pointer ApcRoutine, // _In_opt_
                           Pointer ApcContext, // _In_opt_
                           IpAddrByVal destinationAddress, // _In_
                           Pointer requestData, // _In_
                           short requestSize, // _In_
                           IpOptionInformationByRef requestOptions, // _In_opt_
                           Pointer ReplyBuffer, // _Out_
                           int ReplySize, // _In_
                           int Timeout // _In_
                          );
}

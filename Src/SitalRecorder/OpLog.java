package SitalRecorder;

import java.awt.Font;
import java.awt.GraphicsDevice;
import java.awt.GraphicsEnvironment;
import java.awt.event.WindowAdapter;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JScrollPane;
import javax.swing.JTextPane;
import javax.swing.text.StyledDocument;

/**
 * operative log . records are visible online.
 */
public class OpLog
{
  private JFrame logFrame;
  private JTextPane logTxtPane;
  private StyledDocument doc;
  private int DisplayXPos;
  private int LogWidth;
 
  protected OpLog(int iYPos,String strUKAddr)
  {
    try
    {
      logFrame = new JFrame("["+strUKAddr+"] : SitalRecorder Operative Log");
      try
      { logFrame.setIconImage(new ImageIcon("Images/SitalRecorder32x32.gif").getImage()); }
      catch(Exception ex)
      { System.out.println("OpLog.IconImage: " + ex.toString()); }

      logFrame.addWindowListener(new WindowAdapter()
      {
        @Override
        public void windowClosing(java.awt.event.WindowEvent evt)
        { LogShowOnOff(); }
      });

      DisplayXPos=0;
      LogWidth=1024;
      getInfo4LogShow();
      logFrame.setBounds(DisplayXPos+1,iYPos,LogWidth,400);
      logFrame.setVisible(false);

      logTxtPane = new JTextPane();
      logTxtPane.setFont(new Font(Font.MONOSPACED, Font.TRUETYPE_FONT, 16));
      JScrollPane logScrollPane = new JScrollPane(logTxtPane);
      logScrollPane.setAutoscrolls(true);
      logFrame.add(logScrollPane);
      doc=logTxtPane.getStyledDocument();
    }
    catch (Exception ex)
    { System.out.println("OpLog: " + ex); }
  }

  protected void Exited()
  { logFrame.dispose();  }
  
  protected synchronized boolean isLogShow()
  {
    boolean bShow;
    
    bShow=logFrame.isShowing();
    bShow&=logFrame.isVisible();
    bShow&=(logFrame.getState()==JFrame.NORMAL);
    return (bShow);
  }

  protected synchronized void LogShowOnOff()
  {
    try
    {
      if(isLogShow()) logFrame.setVisible(false);
      else
      {
        logFrame.setVisible(true);
        logFrame.setExtendedState(0);
      }
    }
    catch (Exception ex)
    { System.out.println("LogShow: " + ex); }
  }
  
  //<editor-fold defaultstate="collapsed" desc="getInfo4LogShow">
  /**
   * ������� ���������� true, ���� � ������� 1 �������, false, ���� ������
   * ������. ���������� ������ ������� � ������� �������.
   *
   * @return
   */
  private void getInfo4LogShow()
  {
    try
    {
      GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
      GraphicsDevice[] gs = ge.getScreenDevices();
      if(gs.length > 1)
      {
        DisplayXPos = gs[0].getDisplayMode().getWidth();
        LogWidth = gs[1].getDisplayMode().getWidth();
      }
      else LogWidth=gs[0].getDisplayMode().getWidth();
    }
    catch (Exception ex)
    { System.out.println("getNumberOfDisplays: " + ex); }
    LogWidth=LogWidth*3/5;
  }
  //</editor-fold>

  protected synchronized void JornalClear()
  {
    try
    {
      doc.remove(0,doc.getLength());
      logTxtPane.setCaretPosition(0);
    }
    catch (Exception ex)
    { System.out.println("JornalClear: " + ex); }
  }
  
  protected synchronized void writeRecord2OpLog(String record)
  {
    try
    {
      doc.insertString(doc.getLength(), record, null);
      logTxtPane.setCaretPosition(doc.getLength());
    }
    catch (Exception ex)
    { System.out.println("writeRecord2OpLog: " + ex); }
  }
}

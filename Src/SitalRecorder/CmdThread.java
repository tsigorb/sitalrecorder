package SitalRecorder;

import static SitalRecorder.SitalRecorder.UKRecorder;
import java.io.FileOutputStream;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.NetworkInterface;
import java.net.SocketException;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.channels.DatagramChannel;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.PriorityQueue;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Predicate;
import javax.swing.JOptionPane;
import javax.swing.Timer;

public class CmdThread extends Thread
{
  private class MessageInfo
  {
    byte SN,OpID;
    long lTmStart,lTmCurrent;
    long SSRC;
    MessageInfo()
    {
      SN=0; OpID=0;
      lTmStart=0; lTmCurrent=0;
      SSRC=0xffffffff;
    }
  }

  private class ReciveCmd
  {
    byte btCmdSrc;
    private short CmdId;
    private long lTime;
    ReciveCmd(byte btCmdSrc,short CmdId,long lTime)
    {
      this.btCmdSrc=btCmdSrc;
      this.CmdId=CmdId;
      this.lTime=lTime;
    }
  }

  //<editor-fold defaultstate="collapsed" desc="Params variables">
  /**
   * IP ������. �� �������� � ������� ����, ����� ���� ��������������, � ������
   * ������������� ���������� ������� ��������� � �������. ��� ���� �����������,
   * ��������, ���������� � ���������� 3G ����� ��� ���������� ������� ���
   * ������ TeamViewer
   */
  protected String RecorderIpAddress;
  protected int RecorderPort;
  protected String UkIPadr;
  protected int UKCmdPort;
  protected int SndStoreTime;
  boolean RecorderEnergo;
  //</editor-fold>

  private byte[] mac;
  private NetworkInterface netInt;
  private static InetSocketAddress send2Address;
  private static DatagramChannel cmdChannel;

  private boolean cmdChannelOpened=false;

  //<editor-fold defaultstate="collapsed" desc="timers">
  //</editor-fold>
  protected static Timer IamTimer;

  private ByteBuffer IpPckt;
  private OpLog opLog;
  private int YPos;
  private int UKState,iUKTOut;

  private volatile boolean bRun, bExit,
                           bUK_NoAnswer,bSendRecorderAlive;

  private Calendar clndr;

  private ArrayList<ReciveCmd> rcvCmdList;
  private ArrayList<CurrentCall> CurrentCallList;
  private PriorityBlockingQueue<CurrentCall> bqCalls;
  private CallInfo2DB ThrdCall2DB;

  private MessageInfo MessInfoArr[][];
  private ArrayList<UKSoundPacket> CurrentSoundList[][];
  private PriorityQueue<UKSoundPacket> pqSnd[][];
  private PriorityBlockingQueue<UKSoundPacket> pbqSnd[][];
  private UKSound2DB ThrdSnd2DB;
  
  PriorityBlockingQueue<FindMessageCmd> pbqQuestions;
  PriorityBlockingQueue<FindMessageCmd> pbqAnswer;
  private MessagesDB ThrdMessDB;
  
  PingThread ThrdPing;

  CmdThread(String name)
  {
    super(name);

    UKState=SitalRecorder.c_UKState_Err;
    bRun=true;
    bExit=false;
    bUK_NoAnswer=false;
    bSendRecorderAlive=false;
    ThrdPing=null;
    {
      Param LoggerCfg=new Param();
      UKCmdPort=Integer.decode(LoggerCfg.����_������_��);
      UkIPadr=LoggerCfg.IP_�����_��;
      RecorderIpAddress=LoggerCfg.IP_�����_�����������;
      RecorderPort=Integer.decode(LoggerCfg.����_�����������);
      SndStoreTime=Integer.decode(LoggerCfg.�����_��������_���������);
      UKRecorder.MessageDir=LoggerCfg.����_�_����������;
      UKRecorder.bPathWithDate=LoggerCfg.����_�_������_����.equals("��");
      UKRecorder.MaxAudioFS=Integer.decode(LoggerCfg.����_������_����������)*1048576;
      RecorderEnergo=LoggerCfg.SinfoOS_Energo.equals("��");
      if(!RecorderEnergo)
      { // ����������� ����� ����� ����������� �� ������� SportX
        try
        {
          InetAddress loclndrAddr=InetAddress.getByName(RecorderIpAddress);
          byte btAddr[]=loclndrAddr.getAddress();
          RecorderPort+=(btAddr[3] & 0xff);
        }
        catch(Exception ex) {}
        
      }
    }
    send2Address=new InetSocketAddress(UkIPadr, UKCmdPort);
    if(openCmdChannel())
    {
      rcvCmdList=new ArrayList(50);
      CurrentCallList=new ArrayList(50);
      bqCalls=new PriorityBlockingQueue();

      MessInfoArr=new MessageInfo[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
      CurrentSoundList=new ArrayList[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
      pqSnd=new PriorityQueue[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
      pbqSnd=new PriorityBlockingQueue[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
      for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
      {
        for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
        {
          MessInfoArr[iPult][iOp]=new MessageInfo();
          CurrentSoundList[iPult][iOp]=new ArrayList(50);
          pqSnd[iPult][iOp]=new PriorityQueue();
          pbqSnd[iPult][iOp]=new PriorityBlockingQueue();
        }
      }

      pbqQuestions=new PriorityBlockingQueue();
      pbqAnswer=new PriorityBlockingQueue();

      ThrdCall2DB=new CallInfo2DB("CallInfo2DBThread",bqCalls);
      ThrdSnd2DB=new UKSound2DB("UKSound2DBThread",pbqSnd);
      ThrdMessDB=new MessagesDB(SndStoreTime,pbqQuestions,pbqAnswer);

      IpPckt=ByteBuffer.allocate(1500);
      UKState=SitalRecorder.c_UKState_NoAnswer;

      if(RecorderEnergo)
      {
        IamTimer=new Timer(1000, (java.awt.event.ActionEvent e)->
        {
          if(iUKTOut>=6)
          {
            UKState=SitalRecorder.c_UKState_NoAnswer;
            bUK_NoAnswer=true;
          }
          else
          {
            iUKTOut++;
            if(UKState==SitalRecorder.c_UKState_Ready) bSendRecorderAlive=true;
          }
        });
        IamTimer.setRepeats(true);
      }
    }
  }

  String getUKAddress()
  { return (UkIPadr); }

  void Exited()
  {
    opLog.Exited();
    bRun=false;
    while(!bExit)
    {
      try { Thread.sleep(100); }
      catch(Exception ex)
      { System.out.println("CmdThread.Exited: "+ex.toString()); }
    }
  }

  void LogShowOnOff()
  { opLog.LogShowOnOff(); }

  boolean isLogShow()
  {
    if(opLog!=null) return (opLog.isLogShow());
    return (false);
  }

  int getUKState()
  { return (UKState); }

  private void clndrculateArithmeticSumOfPacket(byte[] buf)
  {
    int sum=0;                   // clndrculate checkSum
    for(int i=0; i<buf.length-1; i++) sum+=buf[i]&0xFF;
    sum=sum&0xFF;                // get 1 byte checksum
    buf[buf.length-1]=(byte)sum; // simple sum of all bytes 
  }

  private String createDateAndTimeString()
  {
    Calendar cal=Calendar.getInstance();
    SimpleDateFormat sdf=new SimpleDateFormat("dd.MM.yyyy HH:mm:ss,SSS");

    return sdf.format(cal.getTime());
  }

  private boolean openCmdChannel()
  {
    cmdChannelOpened=false;
    try
    {
      InetAddress loclndrAddr=InetAddress.getByName(RecorderIpAddress);
      netInt=NetworkInterface.getByInetAddress(loclndrAddr);
      if(netInt!=null)
      {
        mac=netInt.getHardwareAddress();
        cmdChannel=DatagramChannel.open();
        cmdChannel.configureBlocking(false);
        try
        {
          cmdChannel.socket().bind(new InetSocketAddress(RecorderPort));
          cmdChannelOpened=true;
        }
        catch(SocketException socket_ex)
        {
          String msg="<html><p align=\"center\">��������� �����������, ��������, ��� ��������!</p>";
          JOptionPane.showMessageDialog(null,msg,"��������",JOptionPane.WARNING_MESSAGE);
        }
      }
      else
      {
        String msg="<html><p align=\"center\">��� LAN �������� � IP ������� "+RecorderIpAddress+"<br></p>"
                +"<br><p align=\"left\">��������� ��������:<br>1.����������� ������ ����� � ����� ������������</p>"
                +"<p align=\"left\">2.�� ��������� ������� ������</p>";
        JOptionPane.showMessageDialog(null, msg, "������", JOptionPane.ERROR_MESSAGE);
      }
    }
    catch(Exception ex)
    { System.out.println("openCmdChannel: "+ex.toString()); }
    return cmdChannelOpened;
  }

  private boolean isUKAlive()
  {
    return (IpPckt.array()[2]==(byte)0xFE) &&
           (IpPckt.array()[3]==(byte)0xFF);
  }
  
  private boolean AddIpCmdIfNotExist(byte btCmdSrc,short CmdId)
  {
    for(ReciveCmd cmd: rcvCmdList)
    {
      if((cmd.btCmdSrc==btCmdSrc) && (cmd.CmdId==CmdId)) return false;
    }
    rcvCmdList.add(new ReciveCmd(btCmdSrc,CmdId,clndr.getTimeInMillis()));
    return true;
  }

  private void DelIpCmdAfterTOut()
  { // �������� �������� ������ �� ������ ����� 5 ���
    Predicate<ReciveCmd> pi = (x) -> (x.lTime+5000) < clndr.getTimeInMillis();
    rcvCmdList.removeIf(pi);
  }

  private int GetCallId(int Off)
  { return IpPckt.getInt(Off); }
  
  private void EndAllCallsNow()
  {
    boolean bWait;
    long lTm=clndr.getTimeInMillis();
    UKSoundPacket snd;

    for(CurrentCall call: CurrentCallList)
    {
      call.setCallEnd(lTm);
      bqCalls.add(call);
    }
    CurrentCallList.clear();
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
      {
        for(UKSoundPacket sndC: CurrentSoundList[iPult][iOp])
        { pqSnd[iPult][iOp].add(sndC); }
        CurrentSoundList[iPult][iOp].clear();
        while((snd=pqSnd[iPult][iOp].poll())!=null) pbqSnd[iPult][iOp].add(snd);
      }
    }
    do
    { // ����, ���� ��������� �� ���� ��� ���������� � �������� �������� ������
      bWait=false;
      for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
      {
        for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
        {
          if(!pbqSnd[iPult][iOp].isEmpty())
          {
            bWait=true;
            try { Thread.sleep(100); }
            catch(Exception ex) { }
          }
        }
      }
    }
    while(bWait);
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
      { setFileStartTime(iPult,iOp,0); }
    }
  }

  private boolean CallChangeStateIfExist(CurrentCall call0)
  {
    int iInd;
    for(CurrentCall call : CurrentCallList)
    {
      if(call.CallId==call0.CallId)
      {
        iInd=CurrentCallList.indexOf(call);
        if(call.setNewState(call0))
        {
          if(call0.btCallState==0)
          {
            bqCalls.add(call);
            CurrentCallList.remove(iInd);
          }
          else CurrentCallList.set(iInd,call);
        }
        return true;
      }
    }
    return false;
  }

  private void UKCmdPacket()
  {
    byte btCmd;

    btCmd=IpPckt.get(2);
    if(btCmd==123)
    { // ����� ������������� ����������� ����������� �� ��
      try
      {
        byte btBuf[]=new byte[IpPckt.position()-4];
        System.arraycopy(IpPckt.array(),3,btBuf,0,IpPckt.position()-4);
        String str=new String(btBuf, "windows-1251");
        if(str.contains("SitalRecorder REGISTRATION"))
        {
          UKState=SitalRecorder.c_UKState_Ready;
          iUKTOut=0;
        }
        writeStr2log(str);
      }
      catch(Exception ex)
      { System.out.println("UKCmdPacket: "+ex.toString()); }
    }
    else
    {
      byte SN,CmdCode;
      short CmdId;

//      writeBuf2log(IpPckt.array(),IpPckt.position());
      SN=IpPckt.get(4);
      CmdCode=IpPckt.get(7);
      CmdId=IpPckt.order(ByteOrder.LITTLE_ENDIAN).getShort(5);
      if((btCmd & 0xff)==0xFC)
      { // ������ ����� �� �� �� ������� ������
        if(CmdCode==2)
        { // ��������� �����
          if(IpPckt.position()==15)
          { // ������� ������������ ���������� ����
            if(AddIpCmdIfNotExist(btCmd,CmdId))
            {
              CurrentCall call=new CurrentCall(SN,btCmd,
                                               GetCallId(9),
                                               clndr.getTimeInMillis(),null);
              call.setCallStart();
              CurrentCallList.add(call);
            }
          }
        }
        return;
      }
      if((btCmd & 0xff)==0xFD)
      { // ������� ������� �� �� ��� ������
        if(CmdCode==7)
        { // ��������� ����������
          if(AddIpCmdIfNotExist(btCmd,CmdId))
          {
            CurrentCall call=new CurrentCall(SN,btCmd,
                                             GetCallId(8),
                                             clndr.getTimeInMillis(),
                                             IpPckt);
            if(!CallChangeStateIfExist(call))
            {
              if(call.btCallState!=3)
              {
                call.setCallStart();
                CurrentCallList.add(call);
              }
            }
          }
        }
      }
    }
  }

  private int GetInd4Pult(byte SN)
  {
    if(SN==0) return -1;
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      if(SN==MessInfoArr[iPult][0].SN) return(iPult);
    }
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      if(MessInfoArr[iPult][0].SN==0)
      {
        for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
        { MessInfoArr[iPult][iOp].SN=SN; }
        return(iPult);
      }
    }
    return(-1);
  }

  private int GetIndPultFromSSRC(long SSRC)
  {
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      if(SSRC==MessInfoArr[iPult][0].SSRC) return(iPult);
    }
    for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      if(MessInfoArr[iPult][0].SSRC==0xffffffff)
      {
        MessInfoArr[iPult][0].SSRC=SSRC;
        return(iPult);
      }
    }
    return(-1);
  }

  private int GetInd4Op(int iPult,byte OpID)
  {
    if(iPult<0) return(-1);
    for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
    {
      if(OpID==MessInfoArr[iPult][iOp].OpID) return(iOp);
    }
    for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
    {
      if(MessInfoArr[iPult][iOp].OpID==0)
      { MessInfoArr[iPult][iOp].OpID=OpID; return(iPult); }
    }
    return(-1);
  }

  long getFileStartTime(int iPult,int iOp)
  { return MessInfoArr[iPult][iOp].lTmStart; }

  long getFileCurrentTime(int iPult,int iOp)
  { return MessInfoArr[iPult][iOp].lTmCurrent; }
  
  void setFileStartTime(int iPult,int iOp,long lTm)
  {
    MessInfoArr[iPult][iOp].lTmStart=lTm;
    if(lTm==0)
    {
      MessInfoArr[iPult][iOp].OpID=0;
      MessInfoArr[iPult][0].SSRC=0xffffffff;
      for(iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
      {
        if(MessInfoArr[iPult][iOp].OpID!=0) return;
      }
      for(iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
      { MessInfoArr[iPult][iOp].SN=0; }
    }
  }

  void setFileCurrentTime(int iPult,int iOp,long lTm)
  { MessInfoArr[iPult][iOp].lTmCurrent=lTm; }

  private void AddSndPckt2List(int iPult,int iOp,UKSoundPacket sndCur)
  {
    if(RecorderEnergo)
    {
      int iInd=0;
      boolean bAddQueue=false;
      for(UKSoundPacket snd: CurrentSoundList[iPult][iOp])
      {
        if(snd.btOpId==sndCur.btOpId)
        {
          if(snd.NumbPckt==sndCur.NumbPckt)
          {
            if(sndCur.UKSndArr!=null)
            {
              snd.UKSndArr=new byte[sndCur.Lenght];
              System.arraycopy(sndCur.UKSndArr,0,snd.UKSndArr,0,sndCur.Lenght);
            }
            if(sndCur.PultSndArr!=null)
            {
              snd.PultSndArr=new byte[sndCur.Lenght];
              System.arraycopy(sndCur.PultSndArr,0,snd.PultSndArr,0,sndCur.Lenght);
            }
            pqSnd[iPult][iOp].add(snd);
            iInd=CurrentSoundList[iPult][iOp].indexOf(snd);
            bAddQueue=true;
            break;
          }
        }
      }
      if(bAddQueue) CurrentSoundList[iPult][iOp].remove(iInd);
      else CurrentSoundList[iPult][iOp].add(sndCur);
    }
    else pqSnd[iPult][iOp].add(sndCur);
  }

  private void DecodeEnergoPacket()
  {
    byte btPcktId;
    if(IpPckt.get(0)==0x01)
    { // ��������� ����� ������� ������
      btPcktId=IpPckt.get(1);
      switch(btPcktId)
      {
        case 0x16:        // ����� �� ��
          if(isUKAlive()) iUKTOut=0;
          else UKCmdPacket();
          break;
        case 0x17:        // ����� �� ��������� ������ ���������
          pbqQuestions.add(new FindMessageCmd(IpPckt));
          break;
      }
      return;
    }
    if(IpPckt.get(0)==0x00)
    { // �������� ����� ������� ������ SinfoOs_Energo
      int iPult=GetInd4Pult(UKSoundPacket.getSNFromUKSoundPacket(IpPckt));
      int iOp=GetInd4Op(iPult,UKSoundPacket.getOpIdFromUKSoundPacket(IpPckt));
      if((iPult>=0) && (iOp>=0))
      {
        long lTm=clndr.getTimeInMillis();
        if(getFileStartTime(iPult,iOp)==0) setFileStartTime(iPult,iOp,lTm);
        setFileCurrentTime(iPult,iOp,lTm);
        UKSoundPacket snd=new UKSoundPacket(getFileStartTime(iPult,iOp),
                                            lTm,IpPckt,true);
/*writeStr2log("iPult="+Integer.toString(iPult)+
             ", iOp="+Integer.toString(iOp)+
             ", NumbPckt="+Integer.toString(snd.NumbPckt)+
             ", Dir="+Integer.toString(IpPckt.get(13) & 0x01));*/
        AddSndPckt2List(iPult,iOp,snd);
      }
    }
  }

  private void DecodeSinfoOsMassM1SndPckt()
  {
    if(IpPckt.getShort(0)==(short)(0x9008 & 0xffff))
    { // �������� ����� ������� ������ SinfoOs_MassM1
      long SSRC=UKSoundPacket.getSSRCFromUKSoundPacket(IpPckt);
      int iPult=GetIndPultFromSSRC(SSRC);
      if(iPult>=0)
      {
        long lTm=clndr.getTimeInMillis();
        if(getFileStartTime(iPult,0)==0) setFileStartTime(iPult,0,lTm);
        setFileCurrentTime(iPult,0,lTm);
        UKSoundPacket snd=new UKSoundPacket(getFileStartTime(iPult,0),
                                            lTm,IpPckt,false);
        AddSndPckt2List(iPult,0,snd);
      }
    }
  }

  private void receiveIpPacket()
  {
    try
    {
      IpPckt.rewind();
      if(cmdChannel.receive(IpPckt)!=null)
      {
        if(RecorderEnergo) DecodeEnergoPacket();
        else DecodeSinfoOsMassM1SndPckt();
      }
    }
    catch(Exception ex)
    { System.out.println("error in receiveCmdPacket() "+ex.toString()); }
  }

  void CurrentSoundListWorker(int iPult,int iOp,long lTmCur)
  {
    if(lTmCur==0)
    { // ��������� ������ �������� ���������
      if(getFileStartTime(iPult,iOp)==0) return;
      for(UKSoundPacket snd: CurrentSoundList[iPult][iOp]) pqSnd[iPult][iOp].add(snd);
      CurrentSoundList[iPult][iOp].clear();
      while(pqSnd[iPult][iOp].size()>0) pbqSnd[iPult][iOp].add(pqSnd[iPult][iOp].poll());
      pbqSnd[iPult][iOp].add(new UKSoundPacket(getFileStartTime(iPult,iOp),0,null,false));
      setFileStartTime(iPult,iOp,0);
      return;
    }
    for(UKSoundPacket snd: CurrentSoundList[iPult][iOp])
    {
      if(snd.lTmCurrent+500<=lTmCur)
      { // �������� ����� �������� � ������ >=500 ����
        snd.bSend2Queue=true;
        CurrentSoundList[iPult][iOp].set(CurrentSoundList[iPult][iOp].indexOf(snd), snd);
        pqSnd[iPult][iOp].add(snd);
      }
    }
    Predicate<UKSoundPacket> pi = (x) -> x.bSend2Queue;
    CurrentSoundList[iPult][iOp].removeIf(pi);
    while(pqSnd[iPult][iOp].size()>30) pbqSnd[iPult][iOp].add(pqSnd[iPult][iOp].poll());
  }  

  void SetYPos(int iYPos)
  { YPos=iYPos; }

  private void sendPacket2Uk(byte[] buf)
  {
    try
    { cmdChannel.send(ByteBuffer.wrap(buf),send2Address); }
    catch(Exception ex)
    { System.out.println("sendPacket2Uk: "+ex); }
  }

  void sendAnswerIpPacket(FindMessageCmd answ)
  {
    try
    { cmdChannel.send(ByteBuffer.wrap(answ.btBuf),answ.Addr); }
    catch(Exception ex)
    { System.out.println("sendAnswerIpPacket: "+ex); }
  }
  
  private void sendIamAlivePacket2UK()
  {
    byte[] buf=new byte[]
    { // ������ OutBuf.buf ������ ��������� �������������� �����
      0x16, // ������ ����
      (byte)0xFE, // �������
      (byte)0xCA, // ������� � ����� ��� ��
      'S','i','t','a','l','R','e','c','o','r','d','e','r',' ',
      'a','l','i','v','e',
      0           // room for arithmetic sum
    };
    clndrculateArithmeticSumOfPacket(buf);
    sendPacket2Uk(buf);
  }

  private void sendRegisterMyMACcmd()
  {
    if(mac!=null)
    {   // mac can be null in case of wrong Pult IP address setting
      byte[] buf=new byte[]
      { // ������ OutBuf.buf ������ ��������� �������������� �����
        0x16,
        (byte)0xFE,
        (byte)0xC8,
        0, 0, 0, 0, 0, 0, // MAC-�����
        'S','i','t','a','l','R','e','c','o','r','d','e','r',':',' ',
        'R','E','G','I','S','T','E','R',
        0  // ����� � ������� ��� �������������� �����
      };
      System.arraycopy(mac, 0, buf, 3, 6);
      clndrculateArithmeticSumOfPacket(buf);
      sendPacket2Uk(buf);
      iUKTOut=0;
    }
  }

  void OpJornalClear()
  { opLog.JornalClear(); }

  void writeBuf2log(byte buf[],int iLn)
  {
    FileOutputStream fos=null;
    String str="IpPckt: ",
           strInfo=createDateAndTimeString();

    try
    {
      for(int ind=0;ind<iLn;ind++)
      {
        str+=Integer.toHexString(buf[ind] & 0xff);
        if(ind+1<iLn) str+=", ";
      }
      strInfo+=" "+str+"\r\n";
      fos=new FileOutputStream("RecorderLog.txt", true);
      fos.write(strInfo.getBytes());
    }
    catch(Exception ex)
    { System.out.println("writeStr2log: "+ex.toString()); }
    finally
    {
      if(fos!=null)
      {
        try { fos.close(); }
        catch(Exception ex) { }
      }
    }
    if(opLog!=null) opLog.writeRecord2OpLog(strInfo);
  }

  void writeStr2log(String str)
  {
    FileOutputStream fos=null;
    String strInfo=createDateAndTimeString();

    try
    {
      if(str.length()>=2)
      {
        if((str.charAt(0)=='\r')&&(str.charAt(1)=='\n'))
        {
          str=str.substring(2);
          strInfo="\r\n"+strInfo;
        }
      }
      str+="\r\n";
      strInfo+=" "+str;
      fos=new FileOutputStream("RecorderLog.txt", true);
      fos.write(strInfo.getBytes());
    }
    catch(Exception ex)
    { System.out.println("writeStr2log: "+ex.toString()); }
    finally
    {
      if(fos!=null)
      {
        try { fos.close(); }
        catch(Exception ex) { }
      }
    }
    if(opLog!=null) opLog.writeRecord2OpLog(strInfo);
  }

  @Override
  public void run()
  {
    byte sleepDelay=100;

    if(cmdChannelOpened)
    {
      opLog=new OpLog(YPos,UkIPadr);
      ThrdCall2DB.start();
      ThrdSnd2DB.start();
      ThrdMessDB.start();
      if(RecorderEnergo)
      {
        IamTimer.start();
        sendRegisterMyMACcmd();
      }
      else
      {
        UKState=SitalRecorder.c_UKState_NoAnswer;
        try
        {
          InetAddress IPAddr=InetAddress.getByName(UkIPadr);
          ThrdPing=new PingThread("PingThread",IPAddr.getAddress());
          ThrdPing.start();
        }
        catch(Exception ex) {}
      }
      sleepDelay=10;
    }
    while(bRun)
    {
      if(cmdChannelOpened)
      {
        clndr=Calendar.getInstance();
        if(RecorderEnergo)
        {
          DelIpCmdAfterTOut();
          if(bUK_NoAnswer)
          {
            String str="SitalRecorder: ��������� ������ ����������� �� ��";
            writeStr2log(str);
            sendRegisterMyMACcmd();
            EndAllCallsNow();
            bUK_NoAnswer=false;
            bSendRecorderAlive=false;
          }
          if(bSendRecorderAlive)
          {
            sendIamAlivePacket2UK();
            bSendRecorderAlive=false;
          }
        }
        else
        {
          if(ThrdPing.Result) UKState=SitalRecorder.c_UKState_Ready;
          else UKState=SitalRecorder.c_UKState_NoAnswer;
        }
        if(pbqAnswer.size()!=0) sendAnswerIpPacket(pbqAnswer.poll());
        receiveIpPacket();
        for(int iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
        {
          for(int iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
          {
            if(ThrdSnd2DB.getFileStartTimeEnded(iPult,iOp)==getFileStartTime(iPult,iOp))
            { CurrentSoundListWorker(iPult,iOp,0); }
            else
            {
              if(getFileStartTime(iPult,iOp)!=0)
              {
                if(getFileCurrentTime(iPult,iOp)+1000<=clndr.getTimeInMillis())
                { // if current sound time not change >=1 sec, then end sound
                  CurrentSoundListWorker(iPult,iOp,0);
                }
                else CurrentSoundListWorker(iPult,iOp,clndr.getTimeInMillis());
              }
            }
          }
        }
      }
      try { Thread.sleep(sleepDelay); }
      catch(Exception ex) { }
    }
    if(cmdChannelOpened)
    {
      ThrdCall2DB.bRun=false;
      ThrdSnd2DB.bRun=false;
      ThrdMessDB.bRun=false;
      if(!RecorderEnergo) ThrdPing.bRun=false;
      while((!ThrdCall2DB.bExit) ||
            (!ThrdSnd2DB.bExit) ||
            (!ThrdMessDB.bExit) ||
            ((ThrdPing!=null) && !ThrdPing.bExit))
      {
        try { Thread.sleep(100); }
        catch(Exception ex) { }
      }
    }
    bExit=true;
  }
}

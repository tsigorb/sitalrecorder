package SitalRecorder;

import com.sun.jna.Library;
import com.sun.jna.Native;
import com.sun.jna.Pointer;
import com.sun.jna.Structure;
import java.util.Arrays;
import java.util.List;

public interface Winsock2 extends Library
{
  Winsock2 INSTANCE = (Winsock2)Native.loadLibrary("ws2_32",Winsock2.class);

  public static class WSAData extends Structure
  {
    public short version;
    public short highVersion;
    public byte[] description = new byte[256+1];
    public byte[] systemStatus = new byte[256+1];
    public short maxSockets;
    public short maxUdpDg;
    public Pointer vendorInfo;

    @Override
    protected List<String> getFieldOrder()
    {
      return Arrays.asList(new String[]
                           {"version", "highVersion", "description", "systemStatus",
                            "maxSockets", "maxUdpDg", "vendorInfo"}
                          );
    }
  }

  public static class Hostent extends Structure
  {
    public Pointer name;
    public Pointer aliases;
    public short addrType;
    public short length;
    public Pointer addressList;

    @Override
    protected List<String> getFieldOrder()
    {
      return Arrays.asList(new String[]
                           {"name", "aliases", "addrType", "length","addressList"}
                          );
    }
  }

  int WSAStartup(short versionRequested, WSAData wsadata);
  int WSACleanup();
  Hostent gethostbyname(String name);
}

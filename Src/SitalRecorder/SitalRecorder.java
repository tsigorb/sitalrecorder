package SitalRecorder;

import java.awt.Color;
import java.awt.Point;
import java.awt.PopupMenu;
import java.awt.Rectangle;
import java.awt.SystemTray;
import java.awt.TrayIcon;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.text.SimpleDateFormat;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JOptionPane;
import javax.swing.Timer;

//******** ������� ����� IP ������� ������  ***********
public class SitalRecorder extends javax.swing.JFrame
{
  static SitalRecorder UKRecorder;
  static CmdThread cmdThrd;
  private TrayIcon iconTr;
  private SystemTray sysTray;

  SitalRecorder()
  {
    initComponents();

    try
    { setIconImage(new ImageIcon("Images/SitalRecorder32x32.gif").getImage()); }
    catch(Exception ex)
    { System.out.println("SitalRecorder.IconImage: " + ex.toString()); }
    jLblUkStateIndic.setBackground(Color.black);
  }

  void ExitIfNeed()
  {
    Object[] options = { "��", "���!" };
    int n = JOptionPane.showOptionDialog(this,
                             "����� �� ��������� SitalRecorder?",
                             "�������������",
                             JOptionPane.YES_NO_OPTION,
                             JOptionPane.QUESTION_MESSAGE,
                             null, options,options[0]);
    if(n==0)
    {
      setVisible(false);
      cmdThrd.Exited();
      System.exit(0);
    }    
  }
  
  @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jToggleButton1 = new javax.swing.JToggleButton();
        jLblUkStateIndic = new RoundLabel();
        jLblUkStateTxt = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Sital recorder. V1.");
        setLocation(new java.awt.Point(100, 100));
        setResizable(false);
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowClosing(java.awt.event.WindowEvent evt) {
                formWindowClosing(evt);
            }
            public void windowIconified(java.awt.event.WindowEvent evt) {
                formWindowIconified(evt);
            }
        });

        jToggleButton1.setText("��������/������ ���");
        jToggleButton1.setActionCommand("LogOn_Off");
        jToggleButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
        jToggleButton1.setEnabled(false);
        jToggleButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jToggleButton1ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(jLblUkStateIndic, javax.swing.GroupLayout.PREFERRED_SIZE, 45, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(30, 30, 30)
                        .addComponent(jToggleButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLblUkStateTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 238, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE)))
                .addContainerGap())
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addComponent(jLblUkStateIndic, javax.swing.GroupLayout.DEFAULT_SIZE, 43, Short.MAX_VALUE)
                    .addComponent(jToggleButton1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                .addGap(18, 18, 18)
                .addComponent(jLblUkStateTxt, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(15, 15, 15))
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jToggleButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jToggleButton1ActionPerformed
        // TODO add your handling code here:
      cmdThrd.LogShowOnOff();
      if(jToggleButton1.isSelected()) jToggleButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
      else jToggleButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
    }//GEN-LAST:event_jToggleButton1ActionPerformed

    private void formWindowIconified(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowIconified
      try
      {
        if(iconTr!=null)
        {
          setVisible(false);
          sysTray.add(iconTr);
          /*iconTr.displayMessage("������������ ������������ � ����",
                                "��������� ����������",
                                TrayIcon.MessageType.NONE);*/
         }
      }
       catch(Exception ex)
       { System.out.println("WindowIconified: " + ex.toString()); }
    }//GEN-LAST:event_formWindowIconified

    private void formWindowClosing(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowClosing
      ExitIfNeed();
    }//GEN-LAST:event_formWindowClosing

  void CreateRecorderDataPath(long lTm)
  {
    File fP;
    SimpleDateFormat sdf=new SimpleDateFormat("yyyy/MM/dd");
    try
    {
      if(bPathWithDate) fP=new File(MessageDir+"messages/"+sdf.format(lTm));
      else fP=new File(MessageDir+"messages/");
      fP.mkdirs();
    }
    catch(Exception ex)
    { System.out.println("CurrentCall.mkdirs: "+ex.toString()); }
  }

  void InitCmdThread()
  {
    MessageDir="";
    bPathWithDate=false;
    MaxAudioFS=0;
    cmdThrd=new CmdThread("CmdThread");
    if(cmdThrd.getUKState()!=c_UKState_Err)
    {
      UKStateTimer=new Timer(100,(java.awt.event.ActionEvent e)->
      {
        String str;

        switch(cmdThrd.getUKState())
        {
          case 1:
            if(UKState!=c_UKState_NoAnswer)
            {
              str="["+cmdThrd.getUKAddress()+"] : ��� ����� � ��!";
              jLblUkStateTxt.setText(str);
              jLblUkStateIndic.setBackground(Color.red);
              UKState=c_UKState_NoAnswer;
            }
            break;
          case 2:
            if(UKState!=c_UKState_Ready)
            {
              str="["+cmdThrd.getUKAddress()+":"+
                  Integer.toString(cmdThrd.RecorderPort)+
                  "] : ����� � �� �����������!";
              jLblUkStateTxt.setText(str);
              jLblUkStateIndic.setBackground(Color.green);
              UKState=c_UKState_Ready;
            }
            break;
          default:
            if(UKState!=c_UKState_Err)
            {
              jLblUkStateTxt.setText("");
              UKState=c_UKState_Err;
            }
            break;
        }
        if(cmdThrd.isLogShow())
        {
          if(!jToggleButton1.isSelected())
          {
            jToggleButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.LOWERED));
            jToggleButton1.setSelected(true);
          }
        }
        else
        {
          if(jToggleButton1.isSelected())
          {
            jToggleButton1.setBorder(javax.swing.BorderFactory.createBevelBorder(javax.swing.border.BevelBorder.RAISED));
            jToggleButton1.setSelected(false);
          }
        }
      });
      UKStateTimer.setRepeats(true);

      //<editor-fold defaultstate="collapsed" desc="������ � SystemTray icon">
      {
        try
        {
          sysTray=SystemTray.getSystemTray();
          iconTr=new TrayIcon(ImageIO.read(new File("Images/SitalRecorder32x32.gif")),
                              "Sital Recorder");

          iconTr.addActionListener((ActionEvent ev)->
          {
            setVisible(true);
            setState(JFrame.NORMAL);
            sysTray.remove(iconTr);
          });

          iconTr.addMouseMotionListener(new MouseAdapter()
          {
            @Override
            public void mouseMoved(MouseEvent ev)
            {
              iconTr.setToolTip("Sital Recorder");
            }
          });

          iconTr.addMouseListener(new MouseAdapter()
          {
            @Override
            public void mouseClicked(MouseEvent ev)
            {
              if(ev.getClickCount()>1)
              {
                setVisible(true);
                setState(JFrame.NORMAL);
                sysTray.remove(iconTr);
              }
            }
          });
          {
            PopupMenu popup=new PopupMenu();
            final String strLoggerOn="�������� Sital Recorder";
            final String strOpJornalOnOff="����������� ������ On/Off";
            final String strOpJornalClear="�������� ����������� ������";
            final String strExit="����� �� ���������";
            popup.add(strLoggerOn);
            popup.add(strOpJornalOnOff);
            popup.add(strOpJornalClear);
            popup.add(strExit);
            popup.addActionListener((ActionEvent ev)->
            {
              String strAction=ev.getActionCommand();
              switch(strAction)
              {
                case strLoggerOn:
                  setVisible(true);
                  setState(JFrame.NORMAL);
                  sysTray.remove(iconTr);
                  break;
                case strOpJornalOnOff:
                  cmdThrd.LogShowOnOff();
                  break;
                case strOpJornalClear:
                  cmdThrd.OpJornalClear();
                  break;
                case strExit:
                  ExitIfNeed();
                  break;
              }
            });
            iconTr.setPopupMenu(popup);
          }
        }
        catch(Exception ex)
        { System.out.println("Tray icon: "+ex.toString()); }
      }
      //</editor-fold>
    }
  }

  public static void main(String args[])
  {
    UKRecorder=new SitalRecorder();

    //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
    /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look and feel.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
     */
    try {
      for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
        if ("Nimbus".equals(info.getName())) {
          javax.swing.UIManager.setLookAndFeel(info.getClassName());
          break;
        }
      }
    } catch (Exception ex) {
    }
    //</editor-fold>

    java.awt.EventQueue.invokeLater(new Runnable()
    {
      @Override
      public void run()
      {
        UKRecorder.InitCmdThread();
        if(UKRecorder.cmdThrd.getUKState()==c_UKState_Err) System.exit(0);
        else
        {
          UKRecorder.setVisible(true);
          Rectangle rct=UKRecorder.getContentPane().getBounds();
          Point pt=UKRecorder.getContentPane().getLocationOnScreen();
          UKRecorder.cmdThrd.SetYPos(pt.y+rct.height+5);
          UKRecorder.cmdThrd.start();
          UKRecorder.UKStateTimer.start();
        }
      }
    });
  }

  static int c_UKState_Err=0;
  static int c_UKState_NoAnswer=1;
  static int c_UKState_Ready=2;

  private Timer UKStateTimer;
  private int UKState;

  String MessageDir;
  boolean bPathWithDate;
  long MaxAudioFS;

  final static int MaxPults4UK=12;
  final static int MaxOperator=2;

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel jLblUkStateIndic;
    private javax.swing.JLabel jLblUkStateTxt;
    private javax.swing.JToggleButton jToggleButton1;
    // End of variables declaration//GEN-END:variables
}


package SitalRecorder;

import static SitalRecorder.SitalRecorder.UKRecorder;
import java.io.File;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.PriorityBlockingQueue;
import java.util.function.Predicate;
import java.util.stream.Collectors;

public class MessagesDB extends Thread
{
  private class ProcessCmd extends Thread
  {
    FindMessageCmd cmd;
    PriorityBlockingQueue<FindMessageCmd> pbqAnswer;

    ProcessCmd(FindMessageCmd cmd,
               PriorityBlockingQueue<FindMessageCmd> pbqAnswer)
    {
      this.cmd=cmd;
      this.pbqAnswer=pbqAnswer;
    }

    void DeleteOldMessages()
    {
      if(UKRecorder.bPathWithDate) DeleteOldMessagesInDateDirs();
      else DeleteOldMessagesInOneDir();
    }

    void DeleteOldMessagesInOneDir()
    {
      SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy-MM-dd");
      Date dtMsg;
      String strDir=UKRecorder.MessageDir+"Messages";
      String strFName,strDt;
      long lTmMsgOld=(cmd.StoreTime+1)*24;

      lTmMsgOld*=(long)3600000;
      lTmMsgOld=cmd.clndr.getTimeInMillis()-lTmMsgOld;
      try
      {
        for(File myFile : new File(strDir).listFiles())
        {
          if(myFile.isFile())
          {
            strFName=myFile.getName().toLowerCase();
            strDt=strFName;
            //2019-06-10 10_30_45-1a1.wav
            if((strDt.length()>26) &&
               (strDt.charAt(19)=='-') &&
               (strDt.contains(".wav")))
            {
              strDt=strDt.substring(0,10);
              dtMsg=dtFmt.parse(strDt);
              if(lTmMsgOld>dtMsg.getTime())
              {
// !!!��������������� ���� ������ ��������
myFile.renameTo(new File(strDir+"/"+strFName.replace(".wav",".wa_o")));
                //myFile.delete();
              }
            }
          }
        }
      }
      catch(Exception fdEx) { }
    }

    void DeleteOldMessagesInDateDirs()
    {
      SimpleDateFormat dtFmt = new SimpleDateFormat("yyyy.MM.dd");
      Date dtMsg;
      File dir;
      long lTmMsgOld=(cmd.StoreTime+1)*24;

      lTmMsgOld*=(long)3600000;
      lTmMsgOld=cmd.clndr.getTimeInMillis()-lTmMsgOld;
      try
      {
        List<Path> pathList=Files.walk(Paths.get(UKRecorder.MessageDir+"Messages")).
                                       filter(Files::isDirectory).
                                       collect(Collectors.toList());
        for(Path folder: pathList)
        {
          if(folder.getNameCount()>=4)
          {
            String strDir=folder.getName(0)+"/";
            String strDt="";

            for(int i=1;i<4;i++)
            {
              strDir+=folder.getName(i);
              strDt+=folder.getName(i);
              if(i<folder.getNameCount()-1) strDt+=".";
            }
            try
            {
              dtMsg=dtFmt.parse(strDt);
              if(lTmMsgOld>dtMsg.getTime())
              { // �������, ����� ������ �����, ����� � �����������
                try
                { for(File myFile : new File(strDir).listFiles()) myFile.delete(); }
                catch(Exception fdEx) { }
                for(int i=0;i<3;i++)
                { // �������� �������, �������� ������, ����� ���-�����-����
                  try
                  { dir=new File(strDir); dir.delete(); }
                  catch(Exception ddEx) { }
                  if(i<2)
                  {
                    strDir=folder.getName(0)+"/";
                    for(int j=0;j<2-i;j++) strDir+=folder.getName(j+1)+"/";
                  }
                }
              }
            }
            catch(Exception ex) { }
          }
        }
      }
      catch(Exception ex)
      { System.out.println("Find dirs: " + ex.toString()); }
    }

    @Override
    public void run()
    {
      if(cmd.clndr!=null) DeleteOldMessages();
      else
      {
        //!!! ��������� ������� 
        //!!! ���� ������� �����������, �� ������� ���������� ����� "������� ����",
        //!!! � �����, ������������, ��������� ������
        if(pbqAnswer!=null)
        { //!!! ���� ����, �� ��������� � ���������� �����
          //pbqAnswer.add(new FindMessageCmd(cmd.btBuf,cmd.Addr));
        }
      }
    }
  }

  volatile boolean bRun,bExit;
  int SndStoreTime;
  boolean bDeleteOldMessagesStarted;
  Calendar clndr;
  PriorityBlockingQueue<FindMessageCmd> pbqQuestions;
  PriorityBlockingQueue<FindMessageCmd> pbqAnswer;
  
  ArrayList<ProcessCmd> ProceessCmdList;
  
  MessagesDB(int SndStoreTime,
             PriorityBlockingQueue<FindMessageCmd> pbqQuestions,
             PriorityBlockingQueue<FindMessageCmd> pbqAnswer)
  {
    bRun=true; bExit=false;
    this.SndStoreTime=SndStoreTime;
    this.pbqQuestions=pbqQuestions;
    this.pbqAnswer=pbqAnswer;
    ProceessCmdList=new ArrayList();
  }

  void DeleteOldMessages()
  { // �������� ������ ��������� � ��������� ������!!!
    if(SndStoreTime!=0)
    {
      bDeleteOldMessagesStarted=true;
      FindMessageCmd cmd=new FindMessageCmd(Calendar.getInstance(),SndStoreTime);
      ProcessCmd thrCmd=new ProcessCmd(cmd,null);
      ProceessCmdList.add(thrCmd);
      thrCmd.start();
    }
  }

  boolean isProcessCmds()
  {
    for(ProcessCmd thrCmd : ProceessCmdList)
    {
      if(thrCmd.getState()==State.RUNNABLE) return true;
    }
    return false;
  }

  @Override
  public void run()
  {
    ProcessCmd thrCmd;
    Predicate<ProcessCmd> pi = (x) -> (x.getState()==State.TERMINATED);

    clndr=Calendar.getInstance();
    DeleteOldMessages();
    while(bRun)
    {
      clndr=Calendar.getInstance();
      if(bDeleteOldMessagesStarted)
      {
        if(clndr.get(Calendar.HOUR_OF_DAY)>13) bDeleteOldMessagesStarted=false;
      } 
      else
      {
        if(clndr.get(Calendar.HOUR_OF_DAY)==1) DeleteOldMessages();
      }
      if(!pbqQuestions.isEmpty())
      { // ��������� ������� � ��������� ������
        thrCmd=new ProcessCmd(pbqQuestions.poll(),pbqAnswer);
        ProceessCmdList.add(thrCmd);
        thrCmd.start();
      }
      ProceessCmdList.removeIf(pi);
      try { Thread.sleep(100); }
      catch(Exception ex) { }
    }
    while(isProcessCmds())
    {
      try { Thread.sleep(100); }
      catch(Exception ex) { }
    }
    bExit=true;
  }
}

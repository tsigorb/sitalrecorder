package SitalRecorder;

import java.io.BufferedReader;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.Reader;
import java.lang.reflect.Field;
import java.util.ArrayList;
import javax.swing.JOptionPane;

  /**
   * ����� �������� ��� ��������� ��������� ������,
   * ����������� � ini-�����, � �������� ��� �� ������.
   */
  public class Param
  {
    public String IP_�����_��;
    public String ����_������_��;
    public String IP_�����_�����������;
    public String ����_�����������;
    public String �����_��������_���������;
    public String ����_�_����������;
    public String ����_�_������_����;
    public String ����_������_����������;
    public String SinfoOS_Energo;
    
    protected Param()
    {
      IP_�����_��="192.168.1.200";
      ����_������_��="4000";
      IP_�����_�����������="192.168.1.51";
      ����_�����������="5002";
      �����_��������_���������="30";
      ����_�_����������="";
      ����_�_������_����="���";
      ����_������_����������="0";
      SinfoOS_Energo="���";
      readParamFromIniFile();
    }

    /**
     * ������� ������ �������� ��� ���������.
     *
     * @param param ��������� ������ ������, ������� ��������� � ������
     * ����������. ����� ������������ reflection ��� ��������� � ����������.
     * ��� ��������� �������� ������ ��-�� ������������ ������ � ����� ����������.
     */
    private void searchValueString4Parameter(ArrayList<String> stringsFromIniFile,
                                             String param,boolean bNeedValue)
    {
      try
      {
        Field field=getClass().getDeclaredField(param);

        for(String s : stringsFromIniFile)
        {
          s=s.trim();
          if(s.indexOf(param)==0)
          {
            if(s.indexOf("=")>0)
            {
              if(s.length()>s.indexOf("=")+1)
              {
                s = s.substring(s.indexOf("=") + 1);
                s=s.trim();
                if(s.length()>0)
                {
                  field.set(this,s);
                  return;
                }
              }
            }
          }
        }
        if(bNeedValue)
        { JOptionPane.showMessageDialog(null, "<html>� ����� *.ini �� ��������� ����� ���<br>" + param); }
      }
      catch (Exception ex)
      { JOptionPane.showMessageDialog(null, ex.toString()); }
    }

    private void readParamFromIniFile()
    {
      try
      {
        InputStream ins = new FileInputStream("SitalRecorder.ini");
        Reader r = new InputStreamReader(ins, "windows-1251");
        BufferedReader br = new BufferedReader(r);
        String s;
        ArrayList<String> stringsFromIniFile=new ArrayList<String>();

        // read out from file
        while((s=br.readLine())!=null)
        {
          s=s.trim();
          if(s.contains("//")) s=s.substring(0,s.indexOf("//"));
          if(s.contains(";")) s=s.substring(0,s.indexOf(";"));
          if(s.length()>0) stringsFromIniFile.add(s);
        }
        br.close();
        r.close();
        ins.close();
        
        searchValueString4Parameter(stringsFromIniFile,"IP_�����_��",true);
        searchValueString4Parameter(stringsFromIniFile,"����_������_��",true);
        searchValueString4Parameter(stringsFromIniFile,"IP_�����_�����������",true);
        searchValueString4Parameter(stringsFromIniFile,"����_�����������",true);
        searchValueString4Parameter(stringsFromIniFile,"�����_��������_���������",true);
        searchValueString4Parameter(stringsFromIniFile,"����_�_����������",false);
        searchValueString4Parameter(stringsFromIniFile,"����_�_������_����",false);
        searchValueString4Parameter(stringsFromIniFile,"����_������_����������",false);
        searchValueString4Parameter(stringsFromIniFile,"SinfoOS_Energo",true);

        stringsFromIniFile.clear();
        if(!����_�_����������.isEmpty())
        {
          char chSimb=����_�_����������.charAt(����_�_����������.length()-1);
          if((chSimb!='/') && (chSimb!='\\')) ����_�_����������+="/";
        }
      }
      catch (Exception ex)
      { JOptionPane.showMessageDialog(null,ex.toString()+" readParamFromIniFile()"); }
    }
  }

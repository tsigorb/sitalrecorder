package SitalRecorder;

import java.util.concurrent.PriorityBlockingQueue;

public class CallInfo2DB extends Thread
{
  volatile boolean bRun,bExit;
  PriorityBlockingQueue<CurrentCall> bqCalls;

  protected CallInfo2DB(String name,
                        PriorityBlockingQueue<CurrentCall> bqCalls)
  {
    super(name);
    bRun=true;
    bExit=false;
    this.bqCalls=bqCalls;
  }

  @Override
  public void run()
  {
    CurrentCall call;
    while(bRun)
    {
      call=bqCalls.poll();
      if(call!=null)
      {
        call.WriteInfo2File();
      }
      try { Thread.sleep(10); }
      catch(Exception ex)
      { System.out.println("CallInfo2DB.run: "+ex.toString()); }
    }
    while((call=bqCalls.poll())!=null) call.WriteInfo2File();
    bExit=true;
  }
}

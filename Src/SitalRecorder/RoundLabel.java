package SitalRecorder;

import java.awt.*;
import java.awt.geom.*;
import javax.swing.*;

public class RoundLabel extends JLabel
{
  public RoundLabel()
  {
    super();

    // These statements enlarge the button so that it 
    // becomes a circle rather than an oval.
    Dimension size = getPreferredSize();
    size.width = size.height = Math.min(size.width,size.height);
    setPreferredSize(size);
  }

  // Paint the round background and label.
  @Override
  protected void paintComponent(Graphics g)
  {
    g.setColor(getBackground());
//    g.fillArc(0,0, getSize().width-1,getSize().height-1, 0,180);
    g.fillOval(0,0,getSize().width-1,getSize().height-1);

    // This call will paint the label and the focus rectangle.
    super.paintComponent(g);
  }

  // Paint the border of the button using a simple stroke.
  @Override
  protected void paintBorder(Graphics g)
  {
    //g.setColor(getBackground());
    //g.setColor(Color.BLACK);
    //g.drawOval(0, 0, getSize().width-1, getSize().height-1);
  }

  // Hit detection.
  Shape shape;

  @Override
  public boolean contains(int x, int y)
  {
    // If the button has changed size, make a new shape object.
    if((shape==null) || 
       !shape.getBounds().equals(getBounds()))
    {
      shape = new Ellipse2D.Float(0,0,getWidth(),getHeight());
    }
    return shape.contains(x, y);
  }
}

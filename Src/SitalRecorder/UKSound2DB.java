package SitalRecorder;

import static SitalRecorder.SitalRecorder.UKRecorder;
import java.io.OutputStream;
import java.util.concurrent.PriorityBlockingQueue;

public class UKSound2DB extends Thread
{
  private class UKSndFile
  {
    long lFSz,lTmStart;
    OutputStream osFile;
    String strFName;
    byte WavHead[];
  
    UKSndFile(UKSoundPacket snd)
    {
      lFSz=46;          // ������ ��������� WAV-�����
      lTmStart=snd.lTmStart;
      strFName=snd.getSoundFName(UKRecorder.MessageDir);
      if(snd.bEnergo) WavHead=WaveFile.WavHead8kas;
      else WavHead=WaveFile.WavHead8kam;
    }
  }
  
  volatile boolean bRun,bExit;
  long lTmFStartEnded[][];
  UKSndFile sndFiles[][];
  PriorityBlockingQueue<UKSoundPacket> pbqSnd[][];

  protected UKSound2DB(String name,
                       PriorityBlockingQueue<UKSoundPacket> pbqSnd[][])
  {
    super(name);
    bRun=true;
    bExit=false;
    this.pbqSnd=pbqSnd;

    lTmFStartEnded=new long[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
    sndFiles=new UKSndFile[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];
    for(int iRow=0;iRow<SitalRecorder.MaxPults4UK;iRow++)
    {
      for(int iCol=0;iCol<SitalRecorder.MaxOperator;iCol++)
      {
        lTmFStartEnded[iRow][iCol]=0;
        sndFiles[iRow][iCol]=null;
      }
    }
  }

  synchronized long getFileStartTimeEnded(int iPult,int iOp)
  { // ���������� �� ������ CmdThread!
    long lTm=lTmFStartEnded[iPult][iOp];
    lTmFStartEnded[iPult][iOp]=0;
    return lTm;
  }

  synchronized void setFileStartTimeEnded(int iPult,int iOp,long lTm)
  { lTmFStartEnded[iPult][iOp]=lTm;  }

  void WriteSnd2File(int iPult,int iOp,UKSoundPacket snd)
  {
    if(snd!=null)
    { // ��������� ���������������� ������ ��������� ������ � wav-���� ��� ������ � �������� ind
      if((snd.PultSndArr==null) && (snd.UKSndArr==null))
      { // ������� ��������� ���� - ��������� ����
        if(sndFiles[iPult][iOp]!=null)
        {
          if(sndFiles[iPult][iOp].osFile!=null)
          { WaveFile.close(sndFiles[iPult][iOp].osFile,sndFiles[iPult][iOp].strFName); }
          sndFiles[iPult][iOp]=null;
        }
        return;
      }
      if(sndFiles[iPult][iOp]==null)
      {
        sndFiles[iPult][iOp]=new UKSndFile(snd);
        UKRecorder.CreateRecorderDataPath(snd.lTmStart);
        sndFiles[iPult][iOp].osFile=WaveFile.creat(sndFiles[iPult][iOp].strFName,
                                                   sndFiles[iPult][iOp].WavHead);
      }
      else
      {
        if(sndFiles[iPult][iOp].lTmStart!=snd.lTmStart)
        {
          if(sndFiles[iPult][iOp].osFile!=null)
          { WaveFile.close(sndFiles[iPult][iOp].osFile,sndFiles[iPult][iOp].strFName); }
          sndFiles[iPult][iOp]=new UKSndFile(snd);
          UKRecorder.CreateRecorderDataPath(snd.lTmStart);
          sndFiles[iPult][iOp].osFile=WaveFile.creat(sndFiles[iPult][iOp].strFName,
                                                     sndFiles[iPult][iOp].WavHead);
        }
        else
        {
          if(sndFiles[iPult][iOp].osFile==null)
          { sndFiles[iPult][iOp].osFile=WaveFile.append(sndFiles[iPult][iOp].strFName); }
        }
      }
      if(sndFiles[iPult][iOp].osFile!=null)
      {
        byte btSndResultBuf[];

        int iMaxSz=(snd.PultSndArr!=null) ? snd.PultSndArr.length : 0;
        if(snd.UKSndArr!=null) iMaxSz=(iMaxSz<snd.UKSndArr.length) ? snd.UKSndArr.length : iMaxSz;
        if(iMaxSz==0) return;
        if(!snd.bEnergo) btSndResultBuf=snd.UKSndArr;
        else
        {
          btSndResultBuf=new byte[iMaxSz*2];
          for(int ind=0;ind<iMaxSz;ind++)
          {
            if(snd.UKSndArr==null) btSndResultBuf[ind*2]=(byte)(0xd5 & 0xff);
            else
            {
              if(ind<snd.UKSndArr.length) btSndResultBuf[ind*2]=snd.UKSndArr[ind];
              else
              { btSndResultBuf[ind*2]=snd.UKSndArr[snd.UKSndArr.length-1]; }
            }
            if(snd.PultSndArr==null) btSndResultBuf[ind*2+1]=(byte)(0xd5 & 0xff);
            else
            {
              if(ind<snd.PultSndArr.length) btSndResultBuf[ind*2+1]=snd.PultSndArr[ind];
              else
              { btSndResultBuf[ind*2+1]=snd.PultSndArr[snd.PultSndArr.length-1]; }
            }
          }
        }
        WaveFile.write(sndFiles[iPult][iOp].osFile,btSndResultBuf);
        sndFiles[iPult][iOp].lFSz+=btSndResultBuf.length;
        if((UKRecorder.MaxAudioFS>0) && (sndFiles[iPult][iOp].lFSz>=UKRecorder.MaxAudioFS))
        { setFileStartTimeEnded(iPult,iOp,snd.lTmStart); }
      }
    }
    else
    { // ��������� �������� ���� ����������� ���� �� ����
      if(sndFiles[iPult][iOp]!=null)
      {
        if(sndFiles[iPult][iOp].osFile!=null)
        { WaveFile.close(sndFiles[iPult][iOp].osFile,sndFiles[iPult][iOp].strFName); }
        sndFiles[iPult][iOp]=null;
      }
    }
  }

  @Override
  public void run()
  {
    int iPult,iOp;
    UKSoundPacket snd;
/*    long lTmSndNull[][]=new long[SitalRecorder.MaxPults4UK][SitalRecorder.MaxOperator];

    for(iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      for(iOp=0;iOp<SitalRecorder.MaxOperator;iOp++) lTmSndNull[iPult][iOp]=0;
    }*/
    while(bRun)
    {
//      Calendar clndr=Calendar.getInstance();
      for(iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
      {
        for(iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
        {
          snd=pbqSnd[iPult][iOp].peek();
          if(snd!=null)
          {
            snd=pbqSnd[iPult][iOp].poll();
            WriteSnd2File(iPult,iOp,snd);
          }
/*          else
          { // ��� ������� � �������
            if(lTmSndNull[iPult][iOp]==0)
            { lTmSndNull[iPult][iOp]=clndr.getTimeInMillis(); }
            else
            {
              if(clndr.getTimeInMillis()>lTmSndNull[iPult][iOp]+1000)
              { // ��� �������� ������� > 1 ��� - ��������� ���� �� ������ ������
                WriteSnd2File(iPult,iOp,null);
                lTmSndNull[iPult][iOp]=0;
              }
            }
          }*/
        }
      }
      try { Thread.sleep(10); }
      catch(Exception ex) { }
    }
    for(iPult=0;iPult<SitalRecorder.MaxPults4UK;iPult++)
    {
      for(iOp=0;iOp<SitalRecorder.MaxOperator;iOp++)
      {
        while((snd=pbqSnd[iPult][iOp].poll())!=null) WriteSnd2File(iPult,iOp,snd);
        WriteSnd2File(iPult,iOp,null);
      }
    }
    bExit=true;
  }
}

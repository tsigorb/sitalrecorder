package SitalRecorder;

import static SitalRecorder.SitalRecorder.UKRecorder;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.text.SimpleDateFormat;

public class UKSoundPacket implements Comparable<UKSoundPacket>
{
  long lTmStart,lTmCurrent;
  byte SN,btOpId;
  int NumbPckt,Lenght;
  byte UKSndArr[];
  byte PultSndArr[];            // used for SinfoOs_Energo sound packets
  boolean bEnergo,bSend2Queue;

  // used for SinfoOs_MassM1 sound packets
  long SSRC;
  int Profile;

  UKSoundPacket(long lTmStart,long lTmCurrent,ByteBuffer IpPckt,boolean bEnergo)
  {
    this.lTmStart=lTmStart;
    this.lTmCurrent=lTmCurrent;
    this.bEnergo=bEnergo;
    UKSndArr=null;
    PultSndArr=null;
    NumbPckt=0xffff;
    btOpId=0;
    bSend2Queue=false;
    if(IpPckt!=null)
    {
      Lenght=240;  // 30ms * 8000Hz * 1byte
      if(bEnergo)
      {
        NumbPckt=IpPckt.order(ByteOrder.BIG_ENDIAN).getShort(3) & 0xffff;
        btOpId=getOpIdFromUKSoundPacket(IpPckt);
        SN=getSNFromUKSoundPacket(IpPckt);
        byte btDir=(byte)(IpPckt.get(13) & 0x01); // ==0 - from Pult, ==1 - UK
        if(btDir==1)
        {
          UKSndArr=new byte[Lenght];
          System.arraycopy(IpPckt.array(),17,UKSndArr,0,UKSndArr.length);
        }
        else
        {
          PultSndArr=new byte[Lenght];
          System.arraycopy(IpPckt.array(),17,PultSndArr,0,PultSndArr.length);
        }
      }
      else
      {
        NumbPckt=IpPckt.order(ByteOrder.BIG_ENDIAN).getShort(2) & 0xffff;
        UKSndArr=new byte[Lenght];
        SSRC=IpPckt.order(ByteOrder.BIG_ENDIAN).getInt(8) & 0xffffffff;
        Profile=IpPckt.order(ByteOrder.BIG_ENDIAN).getShort(12) & 0xffff;
        System.arraycopy(IpPckt.array(),16,UKSndArr,0,UKSndArr.length);
      }
    }
  }

  static byte getOpIdFromUKSoundPacket(ByteBuffer IpPckt)
  { return IpPckt.get(14); }

  static byte getSNFromUKSoundPacket(ByteBuffer IpPckt)
  { return (byte)((IpPckt.get(13) >> 1) & 0xff); }

  // for SinfoOs_MassM1 sound packets ONLY!!!
  static long getSSRCFromUKSoundPacket(ByteBuffer IpPckt)
  { return IpPckt.order(ByteOrder.BIG_ENDIAN).getInt(8) & 0xffffffff; }

/*
��� �������� ������� SinfoOs_Energo:
������ ����� �������� ������: SN-OperatorChanel-DateTimeStart.wav
�������� ����� ������� � wav-������� � ������ ������,
��� �� ������ ������ ������� �������� ����������, �� ������� - ��������.
*/
  static String getSoundFName(String MessDir,byte SN,byte btOpId,long lTm)
  {
    SimpleDateFormat sdfP=new SimpleDateFormat("yyyy/MM/dd"),
                     sdfN=new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
    String strFP;
    if(UKRecorder.bPathWithDate) strFP=sdfP.format(lTm)+"/";
    else strFP="";
    return MessDir+"messages/"+strFP+
           sdfN.format(lTm)+"-"+
           Integer.toString(SN)+"-"+Integer.toString(btOpId)+
           ".wav";
  }

/*
��� �������� ������� SinfoOs_MassM1:
������ ����� �������� ������: DateTimeStart-Profile.wav
�������� ����� ������� � wav-������� � ������ ����.
*/
  static String getSoundFName(String MessDir,int Profile,long SSRC,long lTm)
  {
    SimpleDateFormat sdfP=new SimpleDateFormat("yyyy/MM/dd"),
                     sdfN=new SimpleDateFormat("yyyy-MM-dd HH_mm_ss");
    String strFP;
    if(UKRecorder.bPathWithDate) strFP=sdfP.format(lTm)+"/";
    else strFP="";
    return MessDir+"messages/"+
           strFP+
           sdfN.format(lTm)+"-"+
           Integer.toHexString(Profile)+
           ".wav";
  }

  String getSoundFName(String MessDir)
  {
    if(bEnergo) return getSoundFName(MessDir,SN,btOpId,lTmStart);
    else return getSoundFName(MessDir,Profile,SSRC,lTmStart);
  }

  @Override
  public int compareTo(UKSoundPacket obj)
  {
    if(NumbPckt>obj.NumbPckt) return(1);
    if(NumbPckt<obj.NumbPckt) return(-1);
    return(0);
  }
}

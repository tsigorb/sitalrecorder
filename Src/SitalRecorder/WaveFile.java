package SitalRecorder;

import java.io.BufferedInputStream;
import java.io.BufferedOutputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.RandomAccessFile;
import java.nio.ByteBuffer;
import java.nio.ByteOrder;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import javax.sound.sampled.UnsupportedAudioFileException;

public class WaveFile
{
  final static byte WavHead8kas[]=
  { // 8000 Hz, a-Law, stereo
'R','I','F','F',
0,0,0,0,                    // riff_leng=filesize()-8
'W','A','V','E','f','m','t',' ',
0x10,0,0,0,                 // dwFormatLeng=sizeof(WavHead0)-20
6,0,                        // wFormatTag (6-aLaw,7-uLaw)
2,0,                        // nChannels (2)
0x40,0x1f,0,0,              // nSamplesPerSec (8000)
(byte)(0x80 & 0xff),0x3e,0,0, // nAvgBytesPerSec (8000*2)
2,0,                        // nBlockAlign (nChannels?wBitsPerSample)/8 =2
0x08,0,                     // wBitsPerSample (8)
  };
  
  final static byte WavHead8kam[]=
  { // 8000 Hz, a-Law, mono
'R','I','F','F',
0,0,0,0,                    // riff_leng=filesize()-8
'W','A','V','E','f','m','t',' ',
0x10,0,0,0,                 // dwFormatLeng=sizeof(WavHead0)-20
6,0,                        // wFormatTag (6-aLaw,7-uLaw)
1,0,                        // nChannels (1)
0x40,0x1f,0,0,              // nSamplesPerSec (8000)
0x40,0x1f,0,0,              // nAvgBytesPerSec (8000*1)
1,0,                        // nBlockAlign (nChannels*wBitsPerSample)/8
0x08,0,                     // wBitsPerSample (8)
  };

  final static byte WavData[]=
  {
'd','a','t','a',
0,0,0,0                     // data_len=filesize()-sizeof(WavHead0)-8
  };

  static void close(OutputStream osFile,String strFName)
  {
    long lFsz;
    int iFsz;
    boolean bOk=false;
    File wvf=null;
    RandomAccessFile rawvf=null;

    try
    {
      osFile.flush();
      osFile.close();
      if(strFName!=null)
      {
        wvf=new File(strFName+"_");
        rawvf=new RandomAccessFile(wvf,"rw");
        lFsz=rawvf.length();
        rawvf.seek(4);        // offset for RIFF lenght
        iFsz=Integer.reverseBytes((int)(lFsz-8));
        rawvf.writeInt(iFsz);
        rawvf.seek(WavHead8kas.length+4);  // offset for data lenght
        iFsz=Integer.reverseBytes((int)(lFsz-WavHead8kas.length-WavData.length));
        rawvf.writeInt(iFsz);
        bOk=true;
      }
    }
    catch(Exception ex)
    { System.out.println("WaveFile.close: "+ex.toString()); }
    finally
    {
      if(rawvf!=null)
      {
        try
        {
          rawvf.close();
          if(bOk && (wvf!=null)) wvf.renameTo(new File(strFName));
        }
        catch(IOException ex) {}
      }
    }
  }
  
  static OutputStream creat(String strFName,byte WavHead[])
  {
    OutputStream os=null;
    try
    {
      Path p=Paths.get(strFName+"_");
      os=new BufferedOutputStream(Files.newOutputStream(p,StandardOpenOption.CREATE));
      os.write(WavHead,0,WavHead.length);
      os.write(WavData,0,WavData.length);
    }
    catch(IOException ex)
    {
      System.out.println("WaveFile.creat: "+ex.toString());
      if(os!=null)
      {
        try { os.close(); }
        catch(IOException ex1) {}
        os=null;
      }
    }
    return os;
  }

  static OutputStream append(String strFName)
  {
    OutputStream os=null;
    try
    {
      Path p=Paths.get(strFName/*+"_"*/);
      os=new BufferedOutputStream(Files.newOutputStream(p,StandardOpenOption.APPEND));
    }
    catch(IOException ex)
    { System.out.println("WaveFile.append: "+ex.toString()); }
    return os;
  }

  private static boolean CheckWaveFormat(byte btRd[],long lFSz,byte WavHead[])
  {
    ByteBuffer bufExmpl=ByteBuffer.wrap(new byte[WavHead.length+WavData.length]);
    System.arraycopy(WavHead,0,bufExmpl.array(),0,WavHead.length);
    System.arraycopy(WavData,0,bufExmpl.array(),WavHead.length,WavData.length);
    bufExmpl.order(ByteOrder.LITTLE_ENDIAN);
    bufExmpl.putInt(4,(int)(lFSz-8));
    bufExmpl.putInt(WavHead.length+4,(int)(lFSz-WavHead.length+WavData.length));
    return (bufExmpl.compareTo(ByteBuffer.wrap(btRd))==0);
  }

  static InputStream open(String strFName,byte WavHead[])
  {
    InputStream is=null;
    try
    {
      File ff1=new File(strFName);
      Path p=Paths.get(strFName);
      long lFSz=ff1.length();
      int iSz=WavHead.length+WavData.length;
      is=new BufferedInputStream(Files.newInputStream(p,StandardOpenOption.READ));
      byte btBuf[]=new byte[iSz];
      if(is.read(btBuf,0,iSz)!=iSz) throw(new UnsupportedAudioFileException());
      if(!CheckWaveFormat(btBuf,lFSz,WavHead)) throw(new UnsupportedAudioFileException());
    }
    catch(Exception ex)
    {
      System.out.println("WaveFile.open: "+ex.toString());
      if(is!=null)
      {
        try { is.close(); }
        catch(IOException ex1) {}
        is=null;
      }
    }
    return is;
  }

  static boolean write(OutputStream os,byte btBuf[])
  {
    try
    { os.write(btBuf,0,btBuf.length); }
    catch(IOException ex)
    { System.out.println("WaveFile.write: "+ex.toString()); }
    return false;
  }

  static int read(InputStream is,byte buf[],int iOff,int iLn)
  {
    int iSz=-1;
    try
    { iSz=is.read(buf, iOff, iLn); }
    catch(IOException ex)
    { System.out.println("WaveFile.read: "+ex.toString()); }
    return iSz;
  }
}

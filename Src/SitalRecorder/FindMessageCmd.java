package SitalRecorder;

import java.net.InetSocketAddress;
import java.nio.ByteBuffer;
import java.util.Calendar;

public class FindMessageCmd implements Comparable<FindMessageCmd>
{
  int CmdStage,StoreTime;
  byte btBuf[];
  InetSocketAddress Addr;
  Calendar clndr;

  private void FindMessageCmdInit()
  {
    CmdStage=-1;
    StoreTime=30;
    btBuf=null;
    Addr=null;
    clndr=null;
  }

  FindMessageCmd(ByteBuffer IpPckt)
  { // ����������� ��� ������� �� ����� ���������
    FindMessageCmdInit();
    btBuf=new byte[IpPckt.position()];
    System.arraycopy(IpPckt.array(),0,btBuf,0,btBuf.length);
    // �������� ����� ��� �������� ����������!!!
    CmdStage=1;
    clndr=null;
  }

  FindMessageCmd(byte Buf[],InetSocketAddress Addr)
  { // ����������� ��� ������ �� ������
    FindMessageCmdInit();
    btBuf=new byte[Buf.length];
    System.arraycopy(Buf,0,btBuf,0,btBuf.length);
    this.Addr=Addr;
    clndr=null;
  }

  FindMessageCmd(Calendar clndr,int StoreTm)
  {
    FindMessageCmdInit();
    StoreTime=StoreTm;
    this.clndr=clndr;
  }

  @Override
  public int compareTo(FindMessageCmd obj)
  {
//    if(NumbPckt>obj.NumbPckt) return(1);
//    if(NumbPckt<obj.NumbPckt) return(-1);
    return(0);
  }
}

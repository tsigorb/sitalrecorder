package SitalRecorder;

import SitalRecorder.Icmp.IcmpEchoReply;
import SitalRecorder.Icmp.IpAddrByVal;
import SitalRecorder.Icmp.IpOptionInformationByRef;
import SitalRecorder.Winsock2.WSAData;
import com.sun.jna.Memory;
import com.sun.jna.Pointer;

public class PingThread extends Thread
{
  volatile boolean bRun,bExit;
  byte IPAddr[];
  boolean Result;

  protected PingThread(String name,byte IPAddr[])
  {
    super(name);
    bRun=true;
    bExit=false;
    this.IPAddr=IPAddr;
    Result=false;
  }

  @Override
  public void run()
  {
    boolean WSAStarted=false;
    WSAData wsadata=new WSAData();
    IpAddrByVal ipaddr=null;
    IpOptionInformationByRef ipOption=null;
    int replyDataSize=0,returnCode;
    Pointer sendData=null;
    Pointer replyData=null;
    Pointer hIcmp=null;

    if(Winsock2.INSTANCE.WSAStartup((short)2,wsadata)==0)
    {
      WSAStarted=true;
      ipaddr=new IpAddrByVal();
      ipaddr.bytes=IPAddr;

      replyDataSize=32+(new IcmpEchoReply()).size()+20;
      sendData=new Memory(32);
        //           12345678901234567890123456789012 
      byte[] data="SitalRecorder ping packet...    ".getBytes();
      sendData.write(0,data,0,32);
      replyData=new Memory(replyDataSize);
      ipOption=new IpOptionInformationByRef();
      ipOption.ttl=(byte) 255;
      ipOption.tos=(byte) 0;
      ipOption.flags=(byte) 0;
      ipOption.optionsSize=(byte) 0;
      ipOption.optionsData=null;
      hIcmp=Icmp.INSTANCE.IcmpCreateFile();
    }
    while(bRun)
    {
      if(WSAStarted)
      {
        returnCode=Icmp.INSTANCE.IcmpSendEcho2(hIcmp,null,null, null,
                                                   ipaddr,sendData,(short)32,ipOption,
                                                   replyData,replyDataSize,1000);
        IcmpEchoReply echoReply=new IcmpEchoReply(replyData);
        Result=((returnCode!=0) && (echoReply.status==0));
      }
      try { Thread.sleep(2000); }
      catch(Exception ex)
      { System.out.println("PingThread.run: "+ex.toString()); }
    }
    if(WSAStarted)
    {
      Icmp.INSTANCE.IcmpCloseHandle(hIcmp);
      Winsock2.INSTANCE.WSACleanup();
    }
    bExit=true;
  }
}
